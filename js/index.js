$(document).ready(function () {
    /* Bootstrap carousel settings */
    $('.carousel').carousel({
        interval: 10000
    });

    /* Active link for content left menu */
    $('.content-left-nav li').click(function(e) {
        e.preventDefault();
        $('.content-left-nav li.active').removeClass('active');
        $(this).addClass('active');
    });

    /* Active link for header main menu */
    $('.main-menu li').click(function(e) {
        e.preventDefault();
        $('.main-menu li.active').removeClass('active');
        $(this).addClass('active');
    });
});
